package com.nullpointerexception.app.placeexchange.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.nullpointerexception.app.placeexchange.R;
import com.nullpointerexception.app.placeexchange.models.City;

import java.util.ArrayList;

/**
 * Created by anair on 7/25/2015.
 */
public class CityAdapter extends ArrayAdapter<City> implements Filterable {

    private Context currentContext;
    private int resourceId;
    private ArrayList<City> resultList;
    private static final String LOG_TAG = CityAdapter.class.getSimpleName();

    public CityAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.currentContext = context;
        this.resourceId = textViewResourceId;


    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public City getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if (null != constraint) {
                    resultList = new ArrayList<City>();
                    resultList.add(new City("Germany"));
                    resultList.add(new City("Italy"));
                    resultList.add(new City("Mumbai"));


                    // This action is synchronous while the above fetch method is async.
                    if(resultList != null) {
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                        notifyDataSetChanged();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();

                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) currentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.autoCompleteTextViewItem);
        Log.d(LOG_TAG, "position: " + position);
        Log.d(LOG_TAG, resultList.get(position).getName());
        tv.setText(resultList.get(position).getName());
        return convertView;

    }
}
