package com.nullpointerexception.app.placeexchange.models;

/**
 * Created by anair on 7/25/2015.
 */
public class City {

    private int id;
    private String name;

    public City(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
