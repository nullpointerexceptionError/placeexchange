package parent;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.mygola.MyGolaApplication;
import com.test.mygola.entity.User;
import com.test.mygola.repository.UserRepository;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyGolaApplication.class)
@WebAppConfiguration
@IntegrationTest
public class LocaliaApplicationTests {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private UserRepository userRepository;
	
	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void contextLoads() throws Exception {
		mockMvc.perform(get("/user")).andExpect(status().isOk());
	}
	
	@Test
	public void getUserOneTest() throws Exception {
		MvcResult result = mockMvc.perform(get("/user/1"))
				.andExpect(status().isOk())
				.andReturn();
		
		String jsonResult = result.getResponse().getContentAsString();
		
		assertThatJson(jsonResult).node("username").isEqualTo("tester");
				
	}
	
	@Test
	public void addAndGetUserTwoTest() throws Exception {
		
		
		String jsonPostContent = "{"
				+ "\"username\" : \"abhi\","
				+ "\"firstname\" : \"Abhishek\","
				+ "\"lastname\" : \"Nair\","
				+ "\"email\" : \"abhishek@gmail.com\","
				+ "\"phone\" : \"8871684775\","
				+ "\"image_url\" : \"http://i.imgur.com/qDSTH2Y.gif\"}";
		
		MvcResult addResult = mockMvc.perform(post("/user")
								.contentType(MediaType.APPLICATION_JSON)
								.content(jsonPostContent))
								.andExpect(status().is(HttpStatus.SC_CREATED))
								.andReturn();
						
		String getLink = addResult.getResponse().getHeader("Location");
		MvcResult result = mockMvc.perform(get(getLink))
				.andExpect(status().isOk())
				.andReturn();
		
		String jsonResult = result.getResponse().getContentAsString();
		
		assertThatJson(jsonResult)
		.node("username").isEqualTo("abhi");
			
	}

}
