package com.test.mygola.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.mygola.entity.Place;
import com.test.mygola.repository.PlaceRepository;

@RestController
public class PlaceController {
	
	PlaceRepository placeRepository;
	
	@Autowired
	public PlaceController(PlaceRepository placeRepository) {
		this.placeRepository = placeRepository;
	}
	
	@RequestMapping(value = "/place")
	public List<Place> getPlaces() {
		return placeRepository.findAll();
	}

}
