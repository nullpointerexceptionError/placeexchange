package com.test.mygola.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.mygola.entity.Place;

public interface PlaceRepository extends JpaRepository<Place, Long> {

}
