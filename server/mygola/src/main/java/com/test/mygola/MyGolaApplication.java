package com.test.mygola;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.test.mygola.repository.UserRepository;


@SpringBootApplication
public class MyGolaApplication {
	
	/*@Bean
	CommandLineRunner init(UserRepository userRepository, PostRepository postRepository, TagRepository tagRepository) {
		return new CommandLineRunner() {
			
			@Override
			public void run(String... args) throws Exception {
				
				createDummyData(userRepository, postRepository, tagRepository);
			}
		};
	}*/
	
	
	
	/*public void createDummyData(UserRepository userRepository, PostRepository postRepository, TagRepository tagRepository) {
		//userRepository.save(new User("tester", "Greg", "Daneils", "greg@test", "9988765421", "http://image.com/indWnnd/"));
		
	}*/
	
    public static void main(String[] args) {
        SpringApplication.run(MyGolaApplication.class, args);
    }
}
