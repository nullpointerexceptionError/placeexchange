package com.test.mygola.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.mygola.entity.City;

public interface CityRepository extends JpaRepository<City, Long> {

}
