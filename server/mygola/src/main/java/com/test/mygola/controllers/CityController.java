package com.test.mygola.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.mygola.entity.City;
import com.test.mygola.repository.CityRepository;

@RestController
public class CityController {
	
	CityRepository cityRepository;
	
	@Autowired
	public CityController(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}
	
	@RequestMapping(value = "/city")
	public List<City> getCities() {
		return cityRepository.findAll();
	}
}
